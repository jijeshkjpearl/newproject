import { Component, OnInit , Inject } from '@angular/core';
import { Validators, AbstractControl, FormBuilder, FormGroup, FormControl , Validator , FormsModule} from '@angular/forms';
import { Router } from '@angular/router';

import { CheckRequiredField } from '../_shared/helpers/form.helper';
import { AuthService } from '../_auth/services/auth.service';

@Component({
  selector: 'app-orders',
  templateUrl: './orders.component.html',
  styleUrls: ['./orders.component.css']
})
export class OrdersComponent implements OnInit {

  constructor(    private authService: AuthService,
    ) { }

    list=[]

  ngOnInit() {
    this.getOrders();
  }


  private getOrders() {
    this.authService.ListOrders().then(
      data => {
        if (data) {
          this.list=data;
          console.log( this.list);
          

        } else {
        }
      },
      err => {
        console.log('---- ERROR ---- ');
        console.log(err);
       
      });
  }



}
