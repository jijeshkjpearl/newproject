import { RouterModule, Routes } from '@angular/router';
import { ModuleWithProviders } from '@angular/core';

import { AuthGuard } from './_auth/guards/auth.guard';

import { DashboardComponent } from './dashboard/dashboard.component';
import { CustomersComponent } from './customers/customers.component';
import { OrdersComponent } from './orders/orders.component';

import { LoginComponent } from './login/login.component';
import { LogoutComponent } from './logout/logout.component';

/*
* Routing for the items feature are stored in the items module file
*/

const routes: Routes = [
    { path: 'customer', component: CustomersComponent  },
    { path: 'dashboard', component: DashboardComponent , canActivate: [AuthGuard] },
    { path: 'login', component: LoginComponent },
  
    { path: 'logout', component: LogoutComponent },
    // { path: '**', redirectTo: '/dashboard', pathMatch: 'full' },
    // { path: '',  redirectTo: '/dashboard', pathMatch: 'full' }, // catch all route
   
    { path: 'orders', component: OrdersComponent  },
    { path: '',   redirectTo: '/customer', pathMatch: 'full' }, // 
    { path: '',   redirectTo: '/orders', pathMatch: 'full' }, // 

];
export const routingModule: ModuleWithProviders = RouterModule.forRoot(routes);
