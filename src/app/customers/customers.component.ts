import { Component, OnInit } from '@angular/core';
import { Validators, AbstractControl, FormBuilder, FormGroup, FormControl , Validator , FormsModule} from '@angular/forms';
import { Router } from '@angular/router';

import { CheckRequiredField } from '../_shared/helpers/form.helper';
import { AuthService } from '../_auth/services/auth.service';
@Component({
  selector: 'app-customers',
  templateUrl: './customers.component.html',
  styleUrls: ['./customers.component.css']
})
export class CustomersComponent implements OnInit {

  
  customerForm: FormGroup;

  processing: Boolean = false;
  error: Boolean = false;
  checkField  = CheckRequiredField;


  constructor(
    private authService: AuthService,
    private router: Router,
  ) { }

  ngOnInit() {
  
      this.initForm();
    
  }

  // checkRequiredClass(frmControl: string) {
  //   const t  = this.customerForm.get()
  //   return {
  //     'required' : false
  //   };
  // }

  onSubmitButtonClicked() {
    this.error  = false;
    this.processing  = false;
    if (this.customerForm.valid) {
      this.addCustomer();
    }
  }

  private addCustomer() {
    this.processing  = true;
    this.authService.addCustomer(this.customerForm.value).then(
      data => {
        if (data) {
          this.handleCustomerSuccess();
        } else {
          this.handleCustomerError();
        }
      },
      err => {
        console.log('---- ERROR ---- ');
        console.log(err);
        this.handleCustomerError();
      });
  }

  private handleCustomerSuccess() {
    this.processing = false;
    this.error  = false;
    this.router.navigate(['/dashboard']);
  }

  private handleCustomerError() {
    this.processing = false;
    this.error  = true;
  }

  private initForm() {
    this.customerForm = new FormGroup({
      name: new FormControl('', [ Validators.required]),
      age: new FormControl('', Validators.required),
      address: new FormControl('', Validators.required),
    });
  }

}
